import logo from './logo.svg';
import './App.css';
import React, {useState, useEffect} from 'react'
import { render } from '@testing-library/react';
import Search from './components/Search'
import VideoDetail from './components/VideoDetail'
import VideoItem from './components/VideoItem'
import VideoList from './components/VideoList'

class App extends React.Component {

  state={
    videos:[],
    currentVideo: null,
     API_KEY:'AIzaSyC_QMAcocIbQw-cGaU0o1RFHAFfWAla4gE',
     baseUrl:'https://www.googleapis.com/youtube/v3'
  }
   


  // getVideos= async ()=>{
  //   // console.log("in getVideos")
	// 	const res = await fetch(this.state.baseUrl+'/search?&key='+this.state.API_KEY+'&q=coldplay&part=snippet',{headers : { 
  //     'Content-Type': 'application/json',
  //     'Accept': 'application/json'
  //    }})
     
  //   .then((response) => response.json())
  //   .then((videos) => {console.log(videos.items)});
  //   // const videos= await res.json()

  //   // console.log(videos.items)
  //   // const response= await fetch(baseUrl)
  //   // const data= await response.json()
  //   // console.log(data.hits)
  //   // setRecipes(data.hits)
  // }

  // componentDidMount(){
  //   // console.log('mounted')
  //   this.getVideos();
  // }


  // useEffect(async () => {
  //   getVideos()
  // }, [])

  onSearch=async (query)=>{
    // console.log(query)
    const res = await fetch(this.state.baseUrl+'/search?&key='+this.state.API_KEY+'&q='+query+'&part=snippet',{headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json'
     }})
     
    .then((response) => response.json())
    .then((videos) => {
          console.log(videos.items)
          this.setState({
            videos:videos.items
          })
        });
  }

  onSelectVideo=(id)=>{
    console.log('currently playing '+id)
		this.setState({ currentVideo: id });
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <Search onSearchVideo={this.onSearch}/>
          <div className="App-content">
            <div className="selected_video">
              {this.state.currentVideo==null ?
                <div style={{width:'100%', height:'515px',textAlign:'center', paddingTop:'180px'}}>
                  Please select a video
                </div>
              :
              <VideoDetail videoId={this.state.currentVideo} />
              }
            </div>
            <div className="video_list">
              <VideoList videos={this.state.videos} onSelectVideo={this.onSelectVideo} />
            </div>
          </div>
        </header>

      </div>
    );
  }
}

export default App;
