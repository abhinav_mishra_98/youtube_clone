import React, { Component } from 'react'
import '../styles/VideoList.css'
import VideoItem from './VideoItem'

export default class VideoList extends Component {
    render() {
        return (
            <div className="videolist">
                {this.props.videos.map(video=>                       
                    <VideoItem key={video.id.videoId} 
                    videoId={video.id.videoId}
                    image={video.snippet.thumbnails.high.url}
                    title={video.snippet.title}
                    onSelectVideo={this.props.onSelectVideo}
                        />
                    )}
            </div>
        )
    }
}
