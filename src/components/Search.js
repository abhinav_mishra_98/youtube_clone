import React, { Component } from 'react'

export default class Search extends Component {
    render() {
        let query=''
        return (
            <div>
                {/* <form className="search-form" onSubmit={getSearch}>
                    <input className="search-bar" style={{marginRight:'10px'}} type="text" value={search} onChange={updateSearch}/>
                    <button className="search-button" type="submit">Search</button>
                </form>      */}
                <form onSubmit={(e) => e.preventDefault()}>
                    <input type="text" placeholder="Search for a video..."  					
                        onChange={(e) => {
                            query = e.target.value;
                        }}/>
                    <button type="submit" onClick={()=>this.props.onSearchVideo(query)}>
                        Search
                    </button>
                </form>
            </div>
        )
    }
}
