import React, { Component } from 'react'
import '../styles/VideoItem.css'
export default class VideoItem extends Component {


    render() {
        const onVideoItemClicked = () => {
            this.props.onSelectVideo(this.props.videoId);
        };
        return (
            <div className="videoitem" onClick={onVideoItemClicked}>
                <div className="videoitem_thumbnail">
                    <img src={this.props.image}/>
                </div>
                <div className="videoitem_details">
                    <h6>{this.props.title}</h6>
                </div>
            </div>
        )
    }
}
