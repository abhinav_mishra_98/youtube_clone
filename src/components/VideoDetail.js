import React, { Component } from 'react'

export default class VideoDetail extends Component {
    render() {
        return (
            <div className="videodetail" >
                <iframe
                    className='mt-2 mb-2 mx-auto'
                    title='test'
                    width='760'
                    height='515'
                    src={`https://www.youtube.com/embed/${this.props.videoId}`}
                    frameBorder='0'
                    allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                />
                
            </div>
        )
    }
}
